# Informe final del modelo

Informe que describe el modelo final que se entregará, generalmente compuesto por uno o más de los modelos construidos durante la vida del proyecto.

## Enfoque analítico

* ¿Qué es la definición de destino?
* Qué son las entradas (descripción)
* ¿Qué tipo de modelo se construyó?

## Descripción de la solución

* Arquitectura de solución simple (fuentes de datos, componentes de la solución, flujo de datos)
* ¿Qué es la salida?

## Datos

* Fuente
* Esquema de datos
* Muestreo
* Selección (fechas, segmentos)
* Estadísticas (recuentos)

## Caracteristicas

* Lista de características sin procesar y derivadas
* Ranking de importancia.

## Algoritmo

* Descripción o imágenes del gráfico de flujo de datos
* ¿Qué aprendizaje se utilizó?
* Hiperparámetros del alumno

## Resultados

* Gráficos ROC / Lift, AUC, R ^ 2, MAPE según corresponda
* Gráficos de rendimiento para barridos de parámetros, si corresponde
